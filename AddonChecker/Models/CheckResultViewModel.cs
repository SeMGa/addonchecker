﻿using AddonChecker.Checks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Models
{
    public class CheckResultViewModel
    {
        public string AddonName { get; set; }
        public int ChecksNumber { get; set; }
        public int FilesNumber { get; set; }
        public List<CheckResult> ChecksResult { get; set; }
    }
}
