﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AddonChecker.Models;
using System.IO;
using System.Threading;
using Microsoft.AspNetCore.Http;
using System.IO.Compression;
using AddonChecker.Checks.items;
using AddonChecker.Checks;
using System.Reflection;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;

namespace AddonChecker.Controllers
{
    public class BaseController : Controller
    {
        private IConfiguration configuration;

        public BaseController(IConfiguration iConfig)
        {
            configuration = iConfig;
        }

        [NonAction]
        private async void LogResult(CheckResultViewModel result)
        {
            try
            {
                MySqlConnection connection = new MySqlConnection
                {
                    ConnectionString = string.Format("server={0};user id={1};password={2};persistsecurityinfo=True;port={3};database={4};CharSet=utf8",
                       configuration.GetSection("DB").GetSection("IP").Value,
                       configuration.GetSection("DB").GetSection("User").Value,
                       configuration.GetSection("DB").GetSection("Password").Value,
                       configuration.GetSection("DB").GetSection("Port").Value,
                       configuration.GetSection("DB").GetSection("Database").Value)
                };
                MySqlCommand insertLogQuery = new MySqlCommand(string.Format("INSERT INTO `logs`(datetime, tips, warnings, errors) VALUES(CURRENT_TIMESTAMP, {0}, {1}, {2})",
                    MySqlHelper.EscapeString(result.ChecksResult.Where(g => g.ItemType == CheckType.TIP).Count().ToString()),
                    MySqlHelper.EscapeString(result.ChecksResult.Where(g => g.ItemType == CheckType.WARNING).Count().ToString()),
                    MySqlHelper.EscapeString(result.ChecksResult.Where(g => g.ItemType == CheckType.ERROR).Count().ToString())), connection);
                await connection.OpenAsync();

                await insertLogQuery.ExecuteNonQueryAsync();

                if (connection != null)
                    connection.Close();
                connection.Dispose();
        }
            catch
            {
                Debug.WriteLine(string.Format("Logging result for {0}.", result.AddonName));
            }
}

        [HttpPost]
        [Route("checkaddon")]
        public async Task<IActionResult> CheckAddon(IFormFile file)
        {
            if (file == null)
                return View("Index");

            try
            {
                List<CheckResult> checksResult = new List<CheckResult>();
                int checksNumber = 0;
                int filesNumber = 0;
                if (file?.Length > 0)
                {
                    using (MemoryStream stream = new MemoryStream())
                    {
                        await file.CopyToAsync(stream, CancellationToken.None);
                        using (ZipArchive zip = new ZipArchive(stream))
                        {
                            foreach (var entry in zip.Entries)
                            {
                                if (!string.IsNullOrEmpty(entry.Name))
                                    filesNumber++;
                            }

                            foreach (Type typeCheck in Assembly.GetExecutingAssembly().GetTypes().Where(mytype => mytype.GetInterfaces().Contains(typeof(ICheck))))
                            {
                                checksNumber++;

                                ICheck check = (ICheck)Activator.CreateInstance(typeCheck);
                                foreach (var entry in zip.Entries)
                                {
                                    if (string.IsNullOrEmpty(entry.Name))
                                        continue;

                                    if (check.FileExtensions.Contains(Path.GetExtension(entry.Name)))
                                    {
                                        using (MemoryStream decompressedStream = new MemoryStream())
                                        {
                                            using (Stream compressStream = entry.Open())
                                            {
                                                await compressStream.CopyToAsync(decompressedStream);
                                            }

                                            using (StreamReader reader = new StreamReader(decompressedStream))
                                            {
                                                reader.BaseStream.Position = 0;
                                                reader.DiscardBufferedData();
                                                check.CheckFile(entry.FullName, reader, ref checksResult);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }

                    CheckResultViewModel result = new CheckResultViewModel
                    {
                        AddonName = Path.GetFileNameWithoutExtension(file.FileName),
                        ChecksNumber = checksNumber,
                        FilesNumber = filesNumber,
                        ChecksResult = checksResult.OrderByDescending(x => (int)(x.ItemType)).ToList()
                    };

                    LogResult(result);

                    return View(result);
                }
            }
            catch
            {
                return View(new CheckResultViewModel
                {
                    AddonName = Path.GetFileNameWithoutExtension(file.FileName),
                    ChecksNumber = 0,
                    FilesNumber = 0,
                    ChecksResult = new List<CheckResult> { new CheckResult { ItemType = CheckType.ERROR, Message = "Error in processing your addon" } }
                });
            }
            return View("Index");
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("privacy")]
        public IActionResult Privacy()
        {
            return View();
        }

        [Route("about")]
        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
