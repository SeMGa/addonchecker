﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks.items
{
    public class SetScriptCheck : ICheck
    {
        public IEnumerable<string> FileExtensions { get { return new string[] { ".lua" }; } }

        public CheckType ItemType { get { return CheckType.WARNING; } }

        private readonly IList<string> sets = new List<string> {
            ":SetScript(\"OnEnter\"", ":SetScript(\"OnEnter\""
        }.AsReadOnly();

        public void CheckFile(string name, StreamReader stream, ref List<CheckResult> checkResults)
        {
            int lineNumber = 1;
            List<CheckResult> results = new List<CheckResult>();
            while (stream.Peek() >= 0)
            {
                string line = stream.ReadLine();
                foreach (string set in sets)
                {
                    if (line.Contains(set))
                        results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("Possible memory leak in {0}:{1}{2}{3}", name, lineNumber, Environment.NewLine, line) });
                }

                lineNumber++;
            }
            checkResults.AddRange(results);
        }
    }
}
