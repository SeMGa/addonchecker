﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks.items
{
    public class TocCheck : ICheck
    {
        private const int filesLimit = 5;

        public IEnumerable<string> FileExtensions { get { return new string[] { ".toc"}; } }

        public CheckType ItemType { get { return CheckType.ERROR; } }

        private bool tocChecked = false;

        public void CheckFile(string name, StreamReader stream, ref List<CheckResult> checkResults)
        {
            List<CheckResult> results = new List<CheckResult>();
            if (tocChecked)
            {
                results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("Found multiple toc files!{0}{1}", Environment.NewLine, name ) });
            }
            else
            {
                int lineNumber = 1;
                int filesCountLua = 0;
                int filesCountXML = 0;
                while (stream.Peek() >= 0)
                {
                    string line = stream.ReadLine();
                    if (!line.StartsWith("##"))
                    {
                        if (line.Contains(".lua"))
                            filesCountLua++;

                        if (line.Contains(".xml"))
                            filesCountXML++;
                    }
                    lineNumber++;
                }
                int summary = filesCountLua + filesCountXML;
                if (summary == 0)
                {
                    results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("No .lua or .xml files linked in {0}:{1}", name, lineNumber) });
                }
                else if (filesCountLua == 1 && filesCountXML == 0)
                {
                    results.Add(new CheckResult { ItemType = CheckType.WARNING, Message = string.Format("Single linked .lua file in {0}:{1}", name, lineNumber) });
                }
                else if (summary > filesLimit)
                {
                    results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("Too many linked .lua and .xml files in {0}{1}{2}", name, Environment.NewLine, "Consider using single .xml") });
                }

                tocChecked = true;
            }
            checkResults.AddRange(results);
        }
    }
}
