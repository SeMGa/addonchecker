﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks.items
{
    public class LocalCallsCheck : ICheck
    {
        public IEnumerable<string> FileExtensions { get { return new string[] { ".lua" }; } }

        public CheckType ItemType { get { return CheckType.TIP; } }

        private Dictionary<string, int> functionCalls = new Dictionary<string, int>();

        private const int functionCallsLimit = 5;

        private readonly IList<string> functions = new List<string> {
            "math.abs",
            "math.acos",
            "math.asin",
            "math.atan",
            "math.ceil",
            "math.cos",
            "math.deg",
            "math.exp",
            "math.floor",
            "math.fmod",
            "math.huge",
            "math.log",
            "math.max",
            "math.maxinteger",
            "math.min",
            "math.mininteger",
            "math.modf",
            "math.pi",
            "math.rad",
            "math.random",
            "math.randomseed",
            "math.sin",
            "math.sqrt",
            "math.tan",
            "math.tointeger",
            "math.type",
            "math.ult"
        }.AsReadOnly();

        public void CheckFile(string name, StreamReader stream, ref List<CheckResult> checkResults)
        {
            int lineNumber = 1;
            List<CheckResult> results = new List<CheckResult>();
            if (functionCalls.Count == 0)
            {
                foreach (string function in functions)
                {
                    functionCalls.Add(function, 0);
                }
            }
            else
            {
                foreach (string function in functions)
                {
                    functionCalls[function] = 0;
                }
            }

            while (stream.Peek() >= 0)
            {
                string line = stream.ReadLine();
                foreach (string functionName in functions)
                {
                    if (line.Contains(functionName))
                        functionCalls[functionName]++;
                }

                lineNumber++;
            }

            foreach(var kv in functionCalls)
            {
                if (kv.Value > functionCallsLimit)
                {
                    results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("Found {0} calls of \"{1}\" in {2}:{3}{4}{5} \"{1}\"", kv.Value, kv.Key, name, lineNumber, Environment.NewLine, "Consider using local") });
                }
            }

            checkResults.AddRange(results);
        }
    }
}
