﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks.items
{
    public class GStateCheck : ICheck
    {
        public IEnumerable<string> FileExtensions { get { return new string[] {".lua", ".xml" }; } }

        public CheckType ItemType { get { return CheckType.WARNING; } }

        public void CheckFile(string name, StreamReader stream, ref List<CheckResult> checkResults)
        {
            int lineNumber = 1;
            List<CheckResult> results = new List<CheckResult>();
            while (stream.Peek() >= 0)
            {
                string line = stream.ReadLine();
                if (line.Contains("_G["))
                    results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("Accessing _G state in {0}:{1}{2}{3}", name, lineNumber, Environment.NewLine, line) });
                lineNumber++;
            }
            checkResults.AddRange(results);
        }
    }
}
