﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks.items
{
    public class ProtectedFunctionsCheck : ICheck
    {
        public IEnumerable<string> FileExtensions { get { return new string[] { ".lua", ".xml" }; } }

        public CheckType ItemType { get { return CheckType.ERROR; } }

        private readonly IList<string> protectedFunctions = new List<string> {
            "AcceptBattlefieldPort", "AcceptProposal", "AcceptTrade", "C_Calendar.AddEvent", "C_Calendar.UpdateEvent", "C_EquipmentSet.UseEquipmentSet", "C_LFGList.ApplyToGroup",
            "C_LFGList.ClearSearchResults", "C_LFGList.CreateListing", "C_LFGList.RemoveListing", "C_LFGList.Search", "C_PetBattles.SkipTurn", "C_PetBattles.UseAbility",
            "C_PetBattles.UseTrap", "C_PetJournal.PickupPet", "C_PetJournal.SummonPetByGUID", "C_UI.Reload", "CancelUnitBuff", "ChangeActionBarPage", "ClearOverrideBindings", "CreateMacro",
            "PetStopAttack", "PickupAction", "PickupCompanion", "PickupMacro", "PickupPetAction", "PickupSpell", "PickupSpellBookItem","SetBinding", "SetBindingClick", "SetBindingItem",
            "SetBindingMacro", "SetBindingSpell", "SetCurrentTitle", "SetOverrideBinding", "SetOverrideBindingClick", "SetOverrideBindingItem", "SetOverrideBindingMacro", "SetOverrideBindingSpell", "UninviteUnit"
        }.AsReadOnly();

        public void CheckFile(string name, StreamReader stream, ref List<CheckResult> checkResults)
        {
            int lineNumber = 1;
            List<CheckResult> results = new List<CheckResult>();
            while (stream.Peek() >= 0)
            {
                string line = stream.ReadLine();
                foreach (string functionName in protectedFunctions)
                {
                    if (line.Contains(functionName))
                        results.Add(new CheckResult { ItemType = ItemType, Message = string.Format("Calling protected function \"{0}\" in {1}:{2}{3}{4}", functionName, name, lineNumber, Environment.NewLine, line) });
                }

                lineNumber++;
            }
            checkResults.AddRange(results);
        }
    }
}
