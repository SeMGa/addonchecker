﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks
{
    public class CheckResult
    {
        public CheckType ItemType { get; set; }
        public string Message { get; set; }
    }
}
