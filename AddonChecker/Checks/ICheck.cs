﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AddonChecker.Checks
{
    public interface ICheck
    {
        IEnumerable<string> FileExtensions { get; }
        CheckType ItemType { get; }
        void CheckFile(string name, StreamReader stream, ref List<CheckResult> checkResults);
    }
}
