﻿#
# Structure for table "logs"
#

CREATE TABLE `logs` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `tips` bigint(20) unsigned NOT NULL DEFAULT '0',
  `warnings` bigint(20) unsigned NOT NULL DEFAULT '0',
  `errors` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
